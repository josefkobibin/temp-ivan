package main

import (
	"fmt"
	"log"

	"github.com/rabbitmq/amqp091-go"
)

func main() {
	conn, err := amqp091.Dial("amqp://guest:guest@localhost:5672")
	if err != nil {
		log.Fatalln("Cannot connect to rabbit", err)
	}

	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalln("Cannot connect to channel", err)
	}

	err = ch.ExchangeDeclare(
		"messages",
		"direct",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalln("Cannot create exchange", err)
	}

	q, err := ch.QueueDeclare(
		"white",
		false,
		true,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalln("Cannot create queue", err)
	}

	ch.QueueBind(
		q.Name,
		"white",
		"messages",
		false,
		nil,
	)

	messages, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	for message := range messages {
		fmt.Println(string(message.Body))
	}

}
