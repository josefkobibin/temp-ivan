package main

import (
	"bufio"
	"context"
	"log"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/rabbitmq/amqp091-go"
)

func main() {
	log.Println("Point ov conflict")
	conn, er := amqp091.Dial("amqp://guest:guest@localhost:5672")
	if er != nil {
		log.Fatalln("Cannot connect to rabbit", er)
	}

	defer conn.Close()
	// хуйный хуй сасный сас
	ch, err := conn.Channel()
	if err != nil {
		log.Fatalln("Cannot connect to channel", err)
	}

	err = ch.ExchangeDeclare(
		"messages",
		"direct",
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		log.Fatalln("Cannot create exchange", err)
	}

	close, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	go func() {
		scanner := bufio.NewScanner(os.Stdin)
		for {
			select {
			case <-close.Done():
				return
			default:
				scanner.Scan()
				message := scanner.Text()
				key := strings.Split(message, " ")[0]
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()

				ch.PublishWithContext(ctx, "messages", key, false, false, amqp091.Publishing{
					ContentType: "text/plain",
					Body:        []byte(strings.Split(message, " ")[1]),
				})
			}
		}

	}()

	<-close.Done()

}
